export type Json =
  | string
  | number
  | boolean
  | null
  | { [key: string]: Json }
  | Json[]

export interface Database {
  public: {
    Tables: {
      astolfo: {
        Row: {
          id: number
          rating: Database["public"]["Enums"]["rating"]
          source: string | null
          file_extension: string | null
          width: number | null
          height: number | null
          tags: string[] | null
          url: string | null
        }
        Insert: {
          id?: number
          rating: Database["public"]["Enums"]["rating"]
          source?: string | null
          file_extension?: string | null
          width?: number | null
          height?: number | null
          tags?: string[] | null
          url?: string | null
        }
        Update: {
          id?: number
          rating?: Database["public"]["Enums"]["rating"]
          source?: string | null
          file_extension?: string | null
          width?: number | null
          height?: number | null
          tags?: string[] | null
          url?: string | null
        }
      }
    }
    Views: {
      random_astolfo: {
        Row: {
          id: number | null
          rating: Database["public"]["Enums"]["rating"] | null
          source: string | null
          file_extension: string | null
          width: number | null
          height: number | null
          tags: string[] | null
          url: string | null
        }
        Insert: {
          id?: number | null
          rating?: Database["public"]["Enums"]["rating"] | null
          source?: string | null
          file_extension?: string | null
          width?: number | null
          height?: number | null
          tags?: string[] | null
          url?: string | null
        }
        Update: {
          id?: number | null
          rating?: Database["public"]["Enums"]["rating"] | null
          source?: string | null
          file_extension?: string | null
          width?: number | null
          height?: number | null
          tags?: string[] | null
          url?: string | null
        }
      }
    }
    Functions: {
      [_ in never]: never
    }
    Enums: {
      rating: "safe" | "questionable" | "explicit"
    }
  }
}
