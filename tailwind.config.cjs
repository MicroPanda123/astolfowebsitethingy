/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{html,js,svelte,ts}'],
  theme: {
    extend: {
      colors: {
        "rating": {
          "safe": "#00FF00",
          "questionable": "#FFFF00",
          "explicit": "#FF0000"
        }
    }},
  },
  plugins: [],
}
